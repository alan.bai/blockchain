package main

import "math"

// p 诚实节点制造下一个节点的概率
// q 攻击者制造下一个节点的概率


// z 诚实节点确认区块数量
// qz = 攻击者最终消除z个区块的落后差距

func AttackerSuccessProbability(q float64, z int) float64 {
	p := 1 - q
	lambda := float64(z) * (q / p)
	sum := 1.0
	for k := 0; k <= z; k++ {
		poisson := math.Exp(-lambda)
		for i:=1; i <= k; i++ {
			poisson *= lambda / float64(i)
		}
		sum -= poisson * (1 - math.Pow(q / p, float64(z - k)))
	}
	return sum
}