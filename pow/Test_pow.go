package main

import (
	"fmt"
)

func main(){
	//start:=time.Now()
	//for i:=0;i<100000000000;i++{
	//	data:=sha256.Sum256([]byte(strconv.Itoa(i)))
	//	fmt.Printf("%d,%x\n",i,data)
	//	fmt.Printf("%s\n",string(data[len(data)-3:]))
	//	if string(data[0:3])=="012"{ //位数的哈希匹配
	//		usedtime:=time.Since(start).Seconds()
	//		fmt.Printf("挖矿成功%d Ms",usedtime)
	//		break
	//	}
	//}

	TestAttack()
}

func TestAttack() {
	fmt.Printf("%f\n",AttackerSuccessProbability(0.02, 1))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.02, 2))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.02, 3))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.02, 4))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.02, 5))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.02, 6))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.1, 5))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.1, 6))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.1, 7))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.49, 1))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.49, 2))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.49, 3))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.49, 4))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.49, 5))
	fmt.Printf("%f\n",AttackerSuccessProbability(0.49, 6))
}
