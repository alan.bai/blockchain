genesis.json = `{
"config": {
"chainId": 20211130,
"homesteadBlock": 0,
"eip150Block": 0,
"eip155Block": 0,
"eip158Block": 0,
"byzantiumBlock": 0,
"constantinopleBlock": 0,
"petersburgBlock": 0,
"istanbulBlock": 0,
"berlinBlock": 0,
"londonBlock": 0
},
"alloc": {},
"coinbase": "0x0000000000000000000000000000000000000000",
"difficulty": "0x20000",
"extraData": "",
"gasLimit": "0x2fefd8",
"nonce": "0x0000000000000042",
"mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000",
"parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
"timestamp": "0x00"
}`

geth --datadir data init genesis.json
geth --datadir data --networkid 20211130 --http --http.api 'web3,eth,net,debug,personal,minner' --http.corsdomain '*' --allow-insecure-unlock console
enode://76e8ae449e493e7fea20306c98628df43119ed54bb8008520f052e9e377649abdceb091938150185b4e8169d6c3781a22dd168a0d0085b62ca2986b95331e9a3@10.10.20.89:30303
admin.addPeer("enode://76e8ae449e493e7fea20306c98628df43119ed54bb8008520f052e9e377649abdceb091938150185b4e8169d6c3781a22dd168a0d0085b62ca2986b95331e9a3@10.10.20.89:30303")
https://remix.ethereum.org/